#!/bin/bash

set -e

# Set up environment

if [[ "$OSTYPE" == "darwin"* ]]; then
  # Mac OS X things
  export DYLD_LIBRARY_PATH="$DYLD_LIBRARY_PATH:$(brew --prefix openssl)/lib"
  export PGHOST=localhost
  export PATH="/Library/PostgreSQL/9.5/bin/:${PATH}"
  PYTHON='~/.pyenv/versions/3.5.2/bin/python3'
else
  # Linux things
  PYTHON=`which python3`
fi

# Verify system settings

if [[ -z "$PYTHON" ]] ; then
    echo "Please install python3 first."
    exit 1
fi
if ! which node > /dev/null 2>&1; then
    echo "Please install nodejs version 6 or 7."
    exit 1
fi

NODE_VERSION=`node --version`
if ! [[ $NODE_VERSION =~ ^v[67]\.[0-9]+\.[0-9]+$ ]]; then
    echo "Please install nodejs version 4 or 6.  $NODE_VERSION installed."
    exit 1
fi

if ! which psql > /dev/null 2>&1; then
    echo "Please install postgresql version 9.5 or higher."
    exit 1
fi

PSQL_VERSION=`psql --version`
if ! [[ $PSQL_VERSION =~ ^psql\ \(PostgreSQL\)\ 9.[56]\.[0-9]+$ ]]; then
    echo "Please install postgresql version 9.5 or higher."
    exit 1
fi

if ! which virtualenv > /dev/null 2>&1; then
    echo "Please install virtualenv"
    exit 1
fi

# Initialize virtualenv
DIR=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
VENV=$DIR/venv
if [ ! -d "$VENV" ]; then
    virtualenv --python "$PYTHON" $VENV
fi

# Install python dependencies
if [ -f "$DIR/shrinkwrap.txt" ] ; then
	  $VENV/bin/pip install -r $DIR/shrinkwrap.txt
else
    $VENV/bin/pip install -r $DIR/requirements.txt
fi
$VENV/bin/pip install -r $DIR/dev-requirements.txt

# Install node dependencies
cd $DIR/{{ project_name }}
npm install
npm run build:dev

if [ ! -e $DIR/{{project_name}}/{{project_name}}/settings.py ]; then
    cp $DIR/{{project_name}}/{{project_name}}/example.settings.py $DIR/{{project_name}}/{{project_name}}/settings.py
fi

$VENV/bin/python $DIR/{{project_name}}/manage.py makemigrations
$VENV/bin/python $DIR/{{project_name}}/manage.py migrate

echo 'Install successful!'
