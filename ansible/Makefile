ifdef vault_password_file
  VAULT = --vault-password-file $(vault_password_file)
else ifdef ANSIBLE_VAULT_PASSWORD_FILE_ITSOTO
  VAULT = --vault-password-file $(ANSIBLE_VAULT_PASSWORD_FILE_CHARLIE)
else
  VAULT = --ask-vault-pass
endif
ifdef tags
  TAGS = --tags $(tags)
endif
ifdef v
	EXTRA = --extra-vars=django_repo_version=$(v)
endif
ifdef user
	USER = --user $(user)
else
	USER = --user deploy
endif
ifdef rootuser
	ROOT_USER = --user $(rootuser)
else
	ROOT_USER = --user root
endif

APP_TAGS = --tags {{project_name}},django
ALL_HOSTS = -i hosts.cfg
PROD_HOSTS = -i hosts.cfg -l {{project_name}}
STAGE_HOSTS = -i hosts.cfg -l {{project_name}}-staging
TESTING_HOSTS = -i hosts.cfg -l {{project_name}}-testing

#
# Building
#

prod:
	ansible-playbook $(PROD_HOSTS) prod.yml $(VAULT) $(EXTRA) $(USER) $(TAGS)

prodapp:
	ansible-playbook $(PROD_HOSTS) prod.yml $(VAULT) $(EXTRA) $(USER) $(APP_TAGS)

prodcode:
	ansible-playbook $(PROD_HOSTS) prod.yml $(VAULT) $(EXTRA) $(USER) --tags djangocode

stage:
	ansible-playbook $(STAGE_HOSTS) stage.yml $(VAULT) $(EXTRA) $(USER) $(TAGS)

stageapp:
	ansible-playbook $(STAGE_HOSTS) stage.yml $(VAULT) $(EXTRA) $(USER) $(APP_TAGS)

stagecode:
	ansible-playbook $(STAGE_HOSTS) stage.yml $(VAULT) $(EXTRA) $(USER) --tags djangocode

testing:
	ansible-playbook $(TESTING_HOSTS) testing.yml $(VAULT) $(EXTRA) $(USER) $(TAGS)

testingapp:
	ansible-playbook $(TESTING_HOSTS) testing.yml $(VAULT) $(EXTRA) $(USER) $(APP_TAGS)

testingcode:
	ansible-playbook $(TESTING_HOSTS) testing.yml $(VAULT) $(EXTRA) $(USER) --tags djangocode

#
# Upgrades and rebooting
#

upgrades:
	ansible-playbook $(ALL_HOSTS) upgrades.yml $(VAULT) $(USER)

reboot:
	ansible-playbook $(ALL_HOSTS) reboot.yml $(VAULT) $(USER)

#
# Restarting (bouncing) services
#

prodbounce:
	ansible-playbook $(PROD_HOSTS) bounce-prod.yml $(VAULT) $(USER)

stagebounce:
	ansible-playbook $(STAGE_HOSTS) bounce-stage.yml $(VAULT) $(USER)

testingbounce:
	ansible-playbook $(TESTING_HOSTS) bounce-stage.yml $(VAULT) $(USER)

#
# First run
#

firstrunprod:
	ansible-playbook $(PROD_HOSTS) firstrun.yml $(ROOT_USER)
	ansible-playbook $(PROD_HOSTS) prod.yml $(VAULT) $(EXTRA)  $(ROOT_USER)

firstrunstage:
	ansible-playbook $(STAGE_HOSTS) firstrun.yml  $(ROOT_USER)
	ansible-playbook $(STAGE_HOSTS) stage.yml $(VAULT) $(EXTRA) $(ROOT_USER)

firstruntesting:
	ansible-playbook $(TESTING_HOSTS) firstrun.yml  $(ROOT_USER)
	ansible-playbook $(TESTING_HOSTS) testing.yml $(VAULT) $(EXTRA) $(ROOT_USER)
