from .prod_settings import *

ALLOWED_HOSTS = ['{% templatetag openvariable %} django_domain {% templatetag closevariable %}']

DEFAULT_FROM_EMAIL = SERVER_EMAIL = "noreply@{% templatetag openvariable %} django_domain {% templatetag closevariable %}"
ADMINS = MANAGERS = [("Charlie DeTar", "cfd@media.mit.edu")]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{% templatetag openvariable %} django_postgres_db {% templatetag closevariable %}',
        'USER': '{% templatetag openvariable %} django_postgres_user {% templatetag closevariable %}',
        'PASSWORD': '{% templatetag openvariable %} django_postgres_password {% templatetag closevariable %}'
    }
}
