# Django webpack, allauth, ansible project template

Starter template for Django with django-allauth, custom user model with email
as login token, webpack with django-webpack-loader, hot-reloading webpack dev
server, scss, and bootstrap.

Usage:

1. Create project
```
django-admin.py startproject --template=https://gitlab.com/cdetar/cfd-django-starter/repository/archive.zip?ref=master --extension=py,yml,cfg,conf,json,sh --name=gitignore,Makefile PROJECT_NAME
```
2. Install dev dependencies
```
./dev_install.sh
```
3. Init git
```
./git_init.sh
```
4. Run project!
```
PROJECT_NAME/dev_server.py
```
