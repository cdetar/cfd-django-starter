#!/bin/sh

set -e

DIR=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
cd $DIR

echo "# {{project_name}}\n\nstarter template" > README.md

echo "Encrypting secrets with ansible-vault."
ansible-vault encrypt ansible/vars/secrets.yml

git init
git submodule add https://gitlab.com/cdetar/cfd-common-roles.git ansible/vendor/cfd-common-roles/
git submodule add https://github.com/nodesource/ansible-nodejs-role.git ansible/vendor/nodesource.node/

mv gitignore .gitignore
git add --all .
git commit -a -m "First commit"

echo "Git inited complete. Self-deleting init script."
rm -f "$DIR/init_git.sh"
